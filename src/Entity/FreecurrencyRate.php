<?php

namespace Drupal\freecurrency\Entity;

use Drupal;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Provides the Freecurrency Rate entity.
 *
 * @ContentEntityType(
 *   id = "freecurrency_rate",
 *   label = @Translation("Freecurrency Rate"),
 *   label_collection = @Translation("Freecurrency Rates"),
 *   label_singular = @Translation("freecurrency rate"),
 *   label_plural = @Translation("freecurrency rates"),
 *   label_count = @PluralTranslation(
 *     singular = "@count freecurrency rate",
 *     plural = "@count freecurrency rates",
 *   ),
 *   base_table = "freecurrency_rate",
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   entity_keys = {
 *     "id" = "code",
 *     "label" = "code",
 *   },
 *   field_ui_base_route = "entity.freecurrency_rate.admin_form",
 * )
 */
class FreecurrencyRate extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * Function for bulk refill.
   *
   * @param array $dataset
   *   Dataset for processing.
   * @param int $changed
   *   Entity modification date.
   *
   * @return classGenerator
   *   The newly created instance is returned.
   */
  public static function bulkRefill(array $dataset, int $changed = null){
    $storage_handler = Drupal::entityTypeManager()->getStorage('freecurrency_rate');
    $instances = $storage_handler->loadByProperties([]);
    $storage_handler->delete($instances);

    foreach ($dataset as $code => $rate) {
      $instance = static::create();
      $instance->set('code', $code);
      $instance->set('rate', $rate);
      if ($changed !== null) {
        $instance->set('changed', $changed);
      }
      if ($instance->save()) {
        yield $instance;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Code'))
      ->setDescription(t('International bank code. Example: "EUR".'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setSetting('max_length', 3)
      ->setDisplayOptions('form', ['type' => 'string_textfield', 'weight' => '5'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['rate'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Rate'))
      ->setDescription(t('Exchange rate.'))
      ->setRequired(TRUE)
      ->setDefaultValue(0)
      ->setDisplayOptions('form', ['type' => 'number', 'weight' => '10'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('Rate Update time.'));

    return $fields;
  }

}
