<?php

namespace Drupal\freecurrency\Entity;

use Drupal;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Provides the Freecurrency Currency entity.
 *
 * @ContentEntityType(
 *   id = "freecurrency_currency",
 *   label = @Translation("Freecurrency Currency"),
 *   label_collection = @Translation("Freecurrency Currencies"),
 *   label_singular = @Translation("freecurrency currency"),
 *   label_plural = @Translation("freecurrency currencies"),
 *   label_count = @PluralTranslation(
 *     singular = "@count freecurrency currency",
 *     plural = "@count freecurrency currencies",
 *   ),
 *   base_table = "freecurrency_currency",
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   entity_keys = {
 *     "id" = "code",
 *     "label" = "name",
 *   },
 *   field_ui_base_route = "entity.freecurrency_currency.admin_form",
 * )
 */
class FreecurrencyCurrency extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * Function for bulk refill.
   *
   * @param array $dataset
   *   Dataset for processing.
   * @param int $changed
   *   Entity modification date.
   *
   * @return classGenerator
   *   The newly created instance is returned.
   */
  public static function bulkRefill(array $dataset, int $changed = null){
    $storage_handler = Drupal::entityTypeManager()->getStorage('freecurrency_currency');
    $instances = $storage_handler->loadByProperties([]);
    $storage_handler->delete($instances);

    foreach ($dataset as $row) {
      $instance = static::create();
      $instance->set('code', $row['code']);
      $instance->set('name', $row['name']);
      $instance->set('name_plural', $row['name_plural']);
      $instance->set('symbol', $row['symbol']);
      $instance->set('symbol_native', $row['symbol_native']);
      $instance->set('decimal_digits', $row['decimal_digits']);
      $instance->set('rounding', $row['rounding']);
      $instance->set('type', $row['type']);
      if ($changed !== null) {
        $instance->set('changed', $changed);
      }
      if ($instance->save()) {
        yield $instance;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Code'))
      ->setDescription(t('International bank code. Example: "EUR".'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setSetting('max_length', 3)
      ->setDisplayOptions('form', ['type' => 'string_textfield', 'weight' => '5'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Official name of the currency. Example: "Euro".'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 127)
      ->setDisplayOptions('form', ['type' => 'string_textfield', 'weight' => '10'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['name_plural'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name Plural'))
      ->setDescription(t('Official name of the currency in the plural. Example: "Euros".'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 127)
      ->setDisplayOptions('form', ['type' => 'string_textfield', 'weight' => '15'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['symbol'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Symbol'))
      ->setDescription(t('Currency symbol. Example: "€".'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 3)
      ->setDisplayOptions('form', ['type' => 'string_textfield', 'weight' => '20'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['symbol_native'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Symbol Native'))
      ->setDescription(t('National currency symbol or abbreviation. For example: "лв.".'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 15)
      ->setDisplayOptions('form', ['type' => 'string_textfield', 'weight' => '25'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['decimal_digits'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Decimal Digits'))
      ->setDescription(t('Decimal Digits.'))
      ->setRequired(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setDefaultValue(0)
      ->setDisplayOptions('form', ['type' => 'number', 'weight' => '30'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['rounding'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Rounding'))
      ->setDescription(t('Rounding.'))
      ->setRequired(TRUE)
      ->setDefaultValue(0)
      ->setDisplayOptions('form', ['type' => 'number', 'weight' => '35'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('Currency type. Example: "fiat".'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 15)
      ->setDefaultValue('fiat')
      ->setDisplayOptions('form', ['type' => 'string_textfield', 'weight' => '40'])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('Сurrency Update time.'));

    return $fields;
  }

}
