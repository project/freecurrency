<?php

namespace Drupal\freecurrency\SeedData;

use Drupal;

/**
 * Generator for creating data for Currencies and Rates.
 */
class SeedController {

  /**
   * Function for generate Currencies.
   *
   * @return null|array
   *   The dataset of currencies.
   */
  public static function generateCurrencies(){
    $config = Drupal::config('currencies.seeding');
    return $config->get('currencies');
  }

  /**
   * Function for generate Rates.
   *
   * @return null|array
   *   The dataset of rates.
   */
  public static function generateRates(){
    $config = Drupal::config('rates.seeding');
    return $config->get('rates');
  }

  /**
   * Function for uninstall seeds.
   */
  public static function uninstall() {
    Drupal::configFactory()->getEditable('currencies.seeding')->delete();
    Drupal::configFactory()->getEditable('rates.seeding')->delete();
  }

}
