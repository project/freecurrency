<?php

namespace Drupal\freecurrency\Plugin\Block;

use Drupal;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides Currency Converter Block.
 *
 * @Block(
 *   id = "freecurrency_converter",
 *   admin_label = @Translation("Freecurrency Converter"),
 *   category = @Translation("Web services")
 * )
 */
class CurrencyConverter extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $result['form'] = Drupal::formBuilder()->getForm('Drupal\freecurrency\Form\CurrencyConverterForm');
    return $result;
  }

}
