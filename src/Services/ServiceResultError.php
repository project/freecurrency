<?php

namespace Drupal\freecurrency\Services;

/**
 * When a service error occurs.
 */
class ServiceResultError {

  public const ERR_CODE_ERROR = 0;
  public const ERR_CODE_EXCEPTION = -1;
  public const ERR_CODE_CLIENT_EXCEPTION = -2;
  public const ERR_CODE_SERVER_EXCEPTION = -3;
  public const ERR_CODE_REQUEST_EXCEPTION = -4;
  public const ERR_CODE_CONNECT_EXCEPTION = -5;
  public const ERR_CODE_TOO_MANY_REDIRECTS_EXCEPTION = -6;
  public const ERR_CODE_JSON_PARSE_ERROR = -7;

  public static $message;
  public static $code;

  public function __construct($message, $code = 0){
    $this->message = $message;
    $this->code = $code;
  }

  public function getMessage(){
    return $this->message;
  }

}
