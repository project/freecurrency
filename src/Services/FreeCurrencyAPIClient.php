<?php

namespace Drupal\freecurrency\Services;

use Drupal\freecurrency\Services\FreecurrencyAPIClientInterface;
use Drupal\freecurrency\Services\ServiceResultError;
use Drupal\freecurrency\Services\ServiceResultOK;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;

/**
 * FreecurrencyAPI Client.
 */
class FreecurrencyAPIClient implements FreecurrencyAPIClientInterface {

  public $url_status;
  public $url_get_currencies;
  public $url_get_rates;

  /**
   * Function for get Service Status.
   *
   * @return bool
   */
  public function getStatus($api_key){
    $result = static::request($this->url_status, $api_key);
    if ($result instanceof ServiceResultOK) {
      return true;
    }
  }

  /**
   * Function for get Currencies.
   *
   * @return classServiceResultOK|classServiceResultError
   */
  public function getCurrencies($api_key){
    $result = static::request($this->url_get_currencies, $api_key);

    if ($result instanceof ServiceResultOK) {
      return new ServiceResultOK(
        $result->getData()['data']
      );
    }

    return $result;
  }

  /**
   * Function for get Rates.
   *
   * @return classServiceResultOK|classServiceResultError
   */
  public function getRates($api_key){
    $result = static::request($this->url_get_rates, $api_key);

    if ($result instanceof ServiceResultOK) {
      return new ServiceResultOK(
        $result->getData()['data']
      );
    }

    return $result;
  }

  /**
   * Internal Function for make Request.
   *
   * @return classServiceResultOK|classServiceResultError
   */
  protected static function request($url, $api_key){
    try {

      $client = new Client();
      $response = $client->get($url . '?apikey=' . $api_key);
      $status = $response->getStatusCode();

      if ($status !== 200) {
        return new ServiceResultError('state: ' . $response->getStatusCode(), ServiceResultError::ERR_CODE_ERROR);
      }

      // state === 200
      $data = json_decode($response->getBody()->getContents(), TRUE);

      if (!$data) {
        return new ServiceResultError('JSON parse error', ServiceResultError::ERR_CODE_JSON_PARSE_ERROR);
      }

      return new ServiceResultOK($data);

    } catch (ClientException           $e) {return new ServiceResultError('client exception (4xx errors): ' . $e->getMessage(), ServiceResultError::ERR_CODE_CLIENT_EXCEPTION);
    } catch (ServerException           $e) {return new ServiceResultError('server exception (5xx errors): ' . $e->getMessage(), ServiceResultError::ERR_CODE_SERVER_EXCEPTION);
    } catch (RequestException          $e) {return new ServiceResultError('request exception: ' .             $e->getMessage(), ServiceResultError::ERR_CODE_REQUEST_EXCEPTION);
    } catch (ConnectException          $e) {return new ServiceResultError('connect exception: ' .             $e->getMessage(), ServiceResultError::ERR_CODE_CONNECT_EXCEPTION);
    } catch (TooManyRedirectsException $e) {return new ServiceResultError('too many redirects exception: ' .  $e->getMessage(), ServiceResultError::ERR_CODE_TOO_MANY_REDIRECTS_EXCEPTION);
    } catch (Exception                 $e) {return new ServiceResultError('exception: ' .                     $e->getMessage(), ServiceResultError::ERR_CODE_EXCEPTION);}
  }

}
