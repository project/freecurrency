<?php

namespace Drupal\freecurrency\Services;

/**
 * FreecurrencyAPI Client interface.
 */
interface FreecurrencyAPIClientInterface {

  /**
   * Function for get Service Status.
   *
   * @return bool
   */
  public function getStatus($api_key);

  /**
   * Function for get Currencies.
   *
   * @return classServiceResultOK|classServiceResultError
   */
  public function getCurrencies($api_key);

  /**
   * Function for get Rates.
   *
   * @return classServiceResultOK|classServiceResultError
   */
  public function getRates($api_key);

}
