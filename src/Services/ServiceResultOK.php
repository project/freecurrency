<?php

namespace Drupal\freecurrency\Services;

/**
 * When the service returns a good response.
 */
class ServiceResultOK {

  public static $data;

  public function __construct($data){
    $this->data = $data;
  }

  public function getData(){
    return $this->data;
  }

}
