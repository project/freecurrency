<?php

namespace Drupal\freecurrency\Services;

use Drupal;
use Drupal\freecurrency\Services\CurrencyConverterInterface;

/**
 * Our own Currency Conversion Service.
 */
class CurrencyConverter implements CurrencyConverterInterface {

  /**
   * Main converter.
   *
   * @param string $code_from
   *   Currency code.
   * @param string $code_to
   *   Currency code.
   * @param float $amount
   *   Convert value.
   *
   * @return float|null
   *   Result value.
   */
  public function convert(string $code_from, string $code_to, float $amount) {
    $rates = Drupal::entityTypeManager()->getStorage('freecurrency_rate')->loadMultiple();

    $rate_from = isset($rates[$code_from]) ? $rates[$code_from]->get('rate')->value : 0;
    $rate_to = isset($rates[$code_to]) ? $rates[$code_to]->get('rate')->value : 0;

    if ($amount && $rate_from && $rate_to) {
      return $amount / $rate_from * $rate_to;
    }
  }

}
