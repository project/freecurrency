<?php

namespace Drupal\freecurrency\Services;

/**
 * Our own Currency Conversion Service interface.
 */
interface CurrencyConverterInterface {

  /**
   * Main converter.
   *
   * @param string $code_from
   *   Currency code.
   * @param string $code_to
   *   Currency code.
   * @param float $amount
   *   Convert value.
   *
   * @return float|null
   *   Result value.
   */
  public function convert(string $code_from, string $code_to, float $amount);

}
