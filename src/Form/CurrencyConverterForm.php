<?php

namespace Drupal\freecurrency\Form;

use Drupal;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\DecimalFormatter;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for Currency Converter.
 */
class CurrencyConverterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'freecurrency_converter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $currencies_instances = Drupal::entityTypeManager()->getStorage('freecurrency_currency')->loadMultiple();
    $rates_instances = Drupal::entityTypeManager()->getStorage('freecurrency_rate')->loadMultiple();
    $decimal_format_settings = DecimalFormatter::defaultSettings();

    $currencies_options = [];
    foreach ($currencies_instances as $currency) {
      if (isset($rates_instances[$currency->get('code')->value])) {
        $currencies_options[$currency->get('code')->value] = $currency->get('code')->value . ': ' . $this->t($currency->get('name')->value);
      }
    }

    $form['from'] = [
      '#type' => "select",
      '#title' => $this->t("Convert from"),
      "#empty_option" => t('- Select -'),
      '#options' => $currencies_options,
      '#required' => TRUE,
    ];

    $form['to'] = [
      '#type' => "select",
      '#title' => $this->t("Convert to"),
      "#empty_option" => t('- Select -'),
      '#options' => $currencies_options,
      '#required' => TRUE,
    ];

    $form['amount'] = [
      '#type' => "number",
      '#title' => $this->t("Amount"),
      '#required' => TRUE,
      '#default_value' => '1',
      '#min' => '0.01',
      '#max' => '1000000',
      '#step' => '0.01',
    ];

    $form['result'] = [
      '#theme' => 'currency_converter_result',
      '#field' => [
        'title' => $this->t("Result"),
        'value' => number_format(
          $form_state->getValue('result'),
          $decimal_format_settings['scale'],
          $decimal_format_settings['decimal_separator'],
          $decimal_format_settings['thousand_separator']
        ),
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Convert'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $result = Drupal::service('freecurrency.converter')->convert(
      $form_state->getValue('from'),
      $form_state->getValue('to'),
      $form_state->getValue('amount')
    );

    Drupal::messenger()->addMessage(t('Conversion has been completed.'));
    $form_state->setValue('result', $result);
    $form_state->setRebuild(TRUE);
  }

}
