<?php

namespace Drupal\freecurrency\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\freecurrency\Entity\FreecurrencyRate;
use Drupal\freecurrency\Services\ServiceResultError;
use Drupal\freecurrency\Services\ServiceResultOK;

/**
 * Form for administering Rates.
 */
class RatesSettingsForm extends FormBase {

  public $api_key;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'freecurrency_rates_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $update_timestamp = Drupal::state()->get('freecurrency.rates_update', 0);
    $this->api_key = $this->config('freecurrency.settings')->get('api_key');

    if (!$this->api_key) {
      Drupal::messenger()->addMessage(t('API Key is not set!'), 'warning');
    }

    $form['updated'] = [
      '#theme' => 'currency_updated',
      '#field' => [
        'title' => $this->t("Update date (Local Date/Time)"),
        'value' => !$update_timestamp ? $this->t('not produced') : Drupal::service('date.formatter')->format($update_timestamp),
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#disabled' => !$this->api_key,
    ];

    $form['view'] = views_embed_view('freecurrency_rates');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $result = Drupal::service('freecurrency.api_client')->getRates($this->api_key);

    if ($result instanceof ServiceResultError) {
      Drupal::messenger()->addMessage(t('Error: %error', ['%error' => $result->getMessage()]), 'error');
    }

    if ($result instanceof ServiceResultOK) {
      $inserted_codes = [];
      foreach (FreecurrencyRate::bulkRefill($result->getData()) as $instance) {
        $inserted_codes[] = $instance->get('code')->value;
      }

      Drupal::messenger()->addMessage(t('The old Currency Rates will be deleted if they were previously downloaded.'));
      Drupal::messenger()->addMessage(t('The following Currency Rates have been added: @codes', ['@codes' => implode(', ', $inserted_codes)]));

      $time = Drupal::time()->getRequestTime();
      Drupal::state()->set('freecurrency.rates_update', $time);
    }
  }

}
