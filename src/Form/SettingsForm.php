<?php

namespace Drupal\freecurrency\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for administering basic settings.
 */
class SettingsForm extends ConfigFormBase {

  public const API_KEY_VALID_CHARACTERS_TITLE = 'a-z, A-Z, 0-9, _';
  public const API_KEY_VALID_CHARACTERS_REGULAR_EXPRESSION = '/^[a-zA-Z0-9_]+$/';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'freecurrency_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['freecurrency.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $service_url = Drupal::getContainer('parameters')->getParameter('freecurrency.url_dashboard');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('<a href="@service_url" target="_blank">Go to FreecurrencyAPI to view your API key.</a> Can contain only the following characters: %characters', ['@service_url' => $service_url, '%characters' => static::API_KEY_VALID_CHARACTERS_TITLE]),
      '#default_value' => $this->config('freecurrency.settings')->get('api_key'),
      '#required' => TRUE,
      '#minlength' => 40,
      '#maxlength' => 64,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (mb_strlen($form['api_key']['#value']) && mb_strlen($form['api_key']['#value']) < $form['api_key']['#minlength']) {
      $form_state->setErrorByName('api_key', $this->t('@name cannot be shorter than %min characters but is currently %length characters long.', ['@name' => $form['api_key']['#title'], '%min' => $form['api_key']['#minlength'], '%length' => mb_strlen($form['api_key']['#value'])]));
      return;
    }

    if (mb_strlen($form['api_key']['#value']) && !preg_match(static::API_KEY_VALID_CHARACTERS_REGULAR_EXPRESSION, $form['api_key']['#value'])) {
      $form_state->setErrorByName('api_key', $this->t('@name can contain only the following characters: %characters', ['@name' => $form['api_key']['#title'], '%characters' => static::API_KEY_VALID_CHARACTERS_TITLE]));
      return;
    }

    if (mb_strlen($form['api_key']['#value'])) {
      if (!Drupal::service('freecurrency.api_client')->getStatus($form['api_key']['#value'])) {
        $form_state->setErrorByName('api_key', $this->t('@name contains an invalid value.', ['@name' => $form['api_key']['#title']]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->hasValue('api_key')) {
        $this->config('freecurrency.settings')
          ->set('api_key', $form_state->getValue('api_key'))
          ->save(TRUE);
    }

    parent::submitForm($form, $form_state);
  }

}
