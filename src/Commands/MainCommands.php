<?php

namespace Drupal\freecurrency\Commands;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\freecurrency\Entity\FreecurrencyCurrency;
use Drupal\freecurrency\Entity\FreecurrencyRate;
use Drupal\freecurrency\SeedData\SeedController;
use Drush\Commands\DrushCommands;
use Drush\Drush;

/**
 * Custom Commands for Drush.
 */
class MainCommands extends DrushCommands {

  /**
   * Any date in the past to indicate that the data is out of date.
   */
  public const DEMO_CHANGED = '2020-01-01 00:00:00';

  /**
   * Seeding the database with initial data for Currencies.
   *
   * @command freecurrency-seeding
   * @aliases fcurs
   * @usage drush freecurrency-seeding
   */
  public function freecurrencySeeding():void {
    $changed = DrupalDateTime::createFromFormat(DrupalDateTime::FORMAT, static::DEMO_CHANGED, 'UTC')->format('U');

    $dataset = SeedController::generateCurrencies();
    if ($dataset) {
      $count = 0;
      Drush::output()->writeln('<info>Old Currencies will be deleted if they were previously downloaded.</info>');
      foreach (FreecurrencyCurrency::bulkRefill($dataset, $changed) as $instance) {
        Drush::output()->writeln('<comment>Creating Currency: ' . $instance->get('code')->value . '</comment>');
        $count++;
      }
      Drush::output()->writeln('<info>'. $count . ' Currency(ies) were generated.</info>');
    }

    $dataset = SeedController::generateRates();
    if ($dataset) {
      $count = 0;
      Drush::output()->writeln('<info>Old Currency Rates will be deleted if they were previously downloaded.</info>');
      foreach (FreecurrencyRate::bulkRefill($dataset, $changed) as $instance) {
        Drush::output()->writeln('<comment>Creating Currency Rate: ' . $instance->get('code')->value . '</comment>');
        $count++;
      }
      $count = Drush::output()->writeln('<info>'. $count . ' Rate(s) were generated.</info>');
    }
  }

}
