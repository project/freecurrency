
# Currency Converter (FreecurrencyAPI)

This module uses the FreecurrencyAPI service to obtain exchange rates
and uses them for calculations. Exchange rates are stored in a local database.
The module represents a block for calculating exchange rates.

Exchange rates are updated at certain intervals.
This update occurs when Cron is started, or it can be done manually in the module's
administrative interface.

Exchange rates are displayed via _View_. You can customize the display as you wish.
Under the hood, `Currencies` and `Currency Rates` are represented as _Entities_.



## Installation Steps

- Enable the module **Currency Converter (FreecurrencyAPI)**.
- Go to section _Administration_ → _Configuration_ → _Web services_ → _Administer
  Currency Converter (FreecurrencyAPI)_
  - Go to the _Settings_ tab and append FreecurrencyAPI Key.
  - Go to the _Currencies_ tab and synchronize the data manually.
  - Go to the _Rates_ tab and synchronize the data manually.
- Go to section _Administration_ → _Reports_ → _Status report_ and run **Cron** to check data synchronization.
- Go to section _Administration_ → _Structure_ → _Block layout_ and
  add a new **Freecurrency Converter** block wherever you want.
- In section _Administration_ → _Structure_ → _Views_
  you can modify Views **Freecurrency Currencies** and **Freecurrency Rates**.



## Demonstration of capabilities

Use the following Drush command to generate data:

    drush freecurrency-seeding



## As a service

You can use the module as a service. To do this, just run the following call:

    \Drupal::service('freecurrency.converter')->convert(
      $value_from,
      $value_to,
      $value_amount
    );



## Requirements

This module requires no modules outside of Drupal core.
